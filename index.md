---
title: TITLE
subtitle: sub
author: me
slideNumber: true
theme: moon
transition: none
backgroundTransition: fade
width: 1600
height: 900
history: true
---

## It's a me

::: notes
Mario
:::

## another slide

> with a quote

## with background {data-background-image="backdrop.jpg"}

## code

```php
$foo = 'bar';
```
