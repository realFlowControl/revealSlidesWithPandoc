index.html: index.md index.css
	docker run --rm -v `pwd`:/data pandoc/core --standalone -f markdown -t revealjs index.md -o index.html --highlight-style=zenburn -c index.css

